function [ StageMatrix,LoadVector ] = GetConstraints( EVstate, Load )
L=length(EVstate);

Para=zeros(L,3);


z=0;
ind=1;
for j=1:L
    if EVstate(j,1)==1
        if z==0
            z=1;
            Para(ind,1)=j;
            Para(ind,3)=Load(j,1);
        end
    else
        if z==1
            Para(ind,2)=j-1;
            ind=ind+1;
            z=0;
        end
    end
end

StageMatrix=zeros(ind-1,L);

for i=1:ind-1
    StageMatrix(i,Para(i,1):Para(i,2))=1;
end

LoadVector=Para(1:ind-1,3);



end


