function [ Loadnew,EVstatenew ] = UpdateLoad_v1( Load,EVstate,t )
%update the EV's remaining charging load and the EV's state

Loadnew=Load;
Loadnew(t,1)=Loadnew(t,1)-1;

EVstatenew=EVstate;
EVstatenew(t,1)=0;

L=96;


% only update the current parking event time stages.
% When the remaining charging load equals to 0, we do not need to consider the charging scheduling of the EV,
% we simply set the EV state to be 0, meaning not need to charge.

ind=1;
while t-ind>0     % update the stages before the current stage t   
    if Loadnew(t-ind,1)>0
        Loadnew(t-ind,1)=Loadnew(t-ind,1)-1;
        if Loadnew(t-ind,1)==0
            EVstatenew(t-ind,1)=0;
        end
        ind=ind+1;
    else
        break;
    end
end

ind=1;
while t+ind<=L    % update the stages after the current stage t
    if Loadnew(t+ind,1)>0
        Loadnew(t+ind,1)=Loadnew(t+ind,1)-1;
        if Loadnew(t+ind,1)==0
            EVstatenew(t+ind,1)=0;
        end
        ind=ind+1;
    else
        break;
    end
end

end



