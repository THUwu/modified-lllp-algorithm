N=27;

M=6;

TimeAvg=zeros(N,M);
TimeStd=zeros(N,M);

ChargingCostAvg=zeros(N,M);
ChargingCostStd=zeros(N,M);

x=zeros(N,1);
Scale={'2','3','4','5','6','7','8','9','10','20','30','40','50','60','70','80','90','100','200','300','400','500','600','700','800','900','1000'};
%Scale={'2','3','4','5','6','7','8','9','10','20','30','40','50','60','70','80','90','100','200','300','400','500','600','700','800','900','1000','2000','3000','4000','5000','6000','7000','8000','9000','10000'};


for i=1:N
    name=strcat(Scale(i),'EVs-0517.mat');
    x(i,1)=str2double(Scale(i));
    load(char(name));
    ChargingCostAvg(i,:)=sta(:,1)';
    ChargingCostStd(i,:)=sta(:,2)';
    TimeAvg(i,:)=sta(:,7)';
    TimeStd(i,:)=sta(:,8)';
end
time_ratio=TimeAvg(:,M)./TimeAvg(:,2);
performance_loss=(ChargingCostAvg(:,1:M-1)-repmat(ChargingCostAvg(:,M),1,M-1))./repmat(ChargingCostAvg(:,M),1,M-1);

a=1;