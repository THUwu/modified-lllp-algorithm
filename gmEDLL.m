function [ charging_cost,wind_use,match_degree,Ct,CalTime ] = gmEDLL( wind,windpre,EVstate,Load,DDL )
charging_cost=0;



[L,Ne]=size(EVstate);

windnew=windpre;
EVstatenew=EVstate;

Ct=zeros(L,Ne);

for i=1:Ne
    for j=1:L
        if Load(j,i)<1
            EVstatenew(j,i)=0;
        end
    end
end
windnew=min(windnew,sum(EVstatenew,2));

tic;

t1=toc();


for t=1:L

    windnew=min(windnew,sum(EVstatenew,2));
    Nt=sum(EVstatenew,2);  
     
     %关联情况下优先充 earliest deadline first, equal deadline: least modified laxity first的车
    if Nt(t,1)>=1&&windnew(t,1)>0%windnew(t,1)<Nt(t,1)&&
        [~,col]=find(EVstatenew(t,:));
        tmp=zeros(Nt(t,1),3);
        for tmpind=1:Nt(t,1)
            tmp(tmpind,1)=col(tmpind);
            zz=0;
            for z=t:t+DDL(t,col(tmpind))-1
                if EVstatenew(z,col(tmpind))==0
                    zz=zz+1;
                end
            end
            tmp(tmpind,2)=DDL(t,col(tmpind))-Load(t,col(tmpind))-zz;
            tmp(tmpind,3)=DDL(t,col(tmpind));
        end
        tmp_sort=sortrows(tmp,[3 2]);
        chargeind=1;
        while windnew(t,1)>0&&Nt(t,1)>0
            No=tmp_sort(chargeind,1);
            if Load(t,No)>0
                windnew(t,1)=windnew(t,1)-1;                         
                [Loadi,EVstatenewi]=UpdateLoad_v1(Load(:,No),EVstatenew(:,No),t);
                Load(:,No)=Loadi;
                EVstatenew(:,No)=EVstatenewi;
                Ct(t,No)=Ct(t,No)+1;
            end
            EVstatenew(t,No)=0;
            Nt(t,1)=Nt(t,1)-1;
            chargeind=chargeind+1;
        end             
    end
%     for i=1:L
%         for j=1:Ne
%             if Load(i,j)<1
%                 EVstatenew(i,j)=0;
%             end
%         end
%     end
end

for t=1:L
    for i=1:Ne
        if EVstatenew(t,i)>0&&Load(t,i)>0
            charging_cost=charging_cost+1;
            [Loadi,EVstatenewi]=UpdateLoad_v1(Load(:,i),EVstatenew(:,i),t);
            Load(:,i)=Loadi;
            EVstatenew(:,i)=EVstatenewi;
            %EVstatenew(t,i)=0;
            Ct(t,i)=Ct(t,i)+1;
        end
    end
end

t2=toc();

CalTime=t2-t1;

[ wind_use, match_degree, charging_cost] = CalZhibiao( wind,Ct );

end









