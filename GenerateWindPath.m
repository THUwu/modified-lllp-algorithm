function [ windpath,windpath_pre ] = GenerateWindPath( wind,Ne ,L)

Pe=10;


std=3;  %#################

windpath=zeros(L,1);
x=windpath;

windpath_pre=zeros(L,1);
xx=windpath_pre;


for i=1:L
    index=mod(i,96);
    if index==0
        index=96;
    end
    x(i,1)= normrnd(wind(index,1),Ne^0.5*0.1*std);
    if x(i,1)<0
        x(i,1)=rand(1)*5;
    end
    
    xx(i,1)= normrnd(x(i,1),x(i,1));
    if xx(i,1)<0
        xx(i,1)=rand(1)*5;
    end
    
    windpath_pre(i,1) = WindState( x(i,1),Ne,Pe);
    windpath(i,1) = WindState( xx(i,1),Ne,Pe);
end


end

