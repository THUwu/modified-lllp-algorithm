function [ charging_cost,wind_use,match_degree,Ct,CalTime ] = mLLLP( wind,wind_pre,EVstate,Load,DDL )
% wind: the real wind power
% wind_pre: the predicted wind power
% C_t: the charging profile

charging_cost=0;

L=96;

[~,Ne]=size(EVstate);

windnew=wind_pre;
EVstatenew=EVstate;

Ct=zeros(length(wind),Ne);

for i=1:Ne
    for j=1:L
        if Load(j,i)<1
            EVstatenew(j,i)=0;
        end
    end
end
windnew=min(windnew,sum(EVstatenew,2));

tic;

t1=toc();

% First, consider the stages with sufficient wind power generation to charging the parking EVs.
windnew1=zeros(length(wind),1);
while sum(windnew1-windnew)~=0
    windnew=min(windnew,sum(EVstatenew,2));
    windnew1=windnew;
    for t=1:length(wind)
        Nt=sum(EVstatenew,2);

        if windnew(t,1)>=Nt(t,1)&&Nt(t,1)>=1
            [~,No]=find(EVstatenew(t,:));
            for i=1:Nt(t,1)
                EVstatenew(t,No(i))=0;
                if Load(t,No(i))>0
                    windnew(t,1)=windnew(t,1)-1;

                    [Loadi,EVstatenewi]=UpdateLoad_v1(Load(:,No(i)),EVstatenew(:,No(i)),t);
                    Load(:,No(i))=Loadi;
                    EVstatenew(:,No(i))=EVstatenewi;

                    Ct(t,No(i))=Ct(t,No(i))+1;
                end
            end
        end
    end
end


% Consider the remaining time stages to charge the EVs to full SOC
for t=1:L
    windnew=min(windnew,sum(EVstatenew,2));
    %Nt=sum(EVstatenew,2); 
    [~,col]=find(EVstatenew(t,:));
    Lt=max(DDL(t,col));
    Ltt=min(Lt+t-1,length(wind));
    st=min(t+1,length(wind));
    
    windnew1=zeros(length(wind),1);
    while sum(windnew1-windnew)~=0
        windnew=min(windnew,sum(EVstatenew,2));
        windnew1=windnew;
        for tt=st:Ltt
            Ntt=sum(EVstatenew,2);

            if windnew(tt,1)>=Ntt(tt,1)&&Ntt(tt,1)>=1
                [~,No]=find(EVstatenew(tt,:));
                for i=1:Ntt(tt,1)
                    EVstatenew(tt,No(i))=0;
                    if Load(tt,No(i))>0
                        windnew(tt,1)=windnew(tt,1)-1;
                        %Nt(t,1)=Nt(t,1)-1;  优化UpdateLoad_v1函数
                        [Loadi,EVstatenewi]=UpdateLoad_v1(Load(:,No(i)),EVstatenew(:,No(i)),tt);
                        Load(:,No(i))=Loadi;
                        EVstatenew(:,No(i))=EVstatenewi;
                        Ct(tt,No(i))=Ct(tt,No(i))+1;
                    end
                end
            end
        end 
    end
    
    windnew=min(windnew,sum(EVstatenew,2));
    
    Nt=sum(EVstatenew,2);  
     
     %关联情况下优先充 earliest deadline first, equal deadline: least modified laxity first的车
    if Nt(t,1)>=1&&windnew(t,1)>0%windnew(t,1)<Nt(t,1)&&
        [~,col]=find(EVstatenew(t,:));
        tmp=zeros(Nt(t,1),4);
        for tmpind=1:Nt(t,1)
            tmp(tmpind,1)=col(tmpind);
            zz=0;
            for z=t:t+DDL(t,col(tmpind))-1
                if EVstatenew(z,col(tmpind))==0||windnew(z,1)==0
                    zz=zz+1;
                end
            end
            tmp(tmpind,2)=DDL(t,col(tmpind))-Load(t,col(tmpind))-zz;
            tmp(tmpind,3)=Load(t,col(tmpind));
            tmp(tmpind,4)=DDL(t,col(tmpind));
        end
        tmp_sort=sortrows(tmp,[2 -3 4]);
        chargeind=1;
        while windnew(t,1)>0&&Nt(t,1)>0
            No=tmp_sort(chargeind,1);
            if Load(t,No)>0
                windnew(t,1)=windnew(t,1)-1;                         
                [Loadi,EVstatenewi]=UpdateLoad_v1(Load(:,No),EVstatenew(:,No),t);
                Load(:,No)=Loadi;
                EVstatenew(:,No)=EVstatenewi;
                Ct(t,No)=Ct(t,No)+1;
            end
            EVstatenew(t,No)=0;
            Nt(t,1)=Nt(t,1)-1;
            chargeind=chargeind+1;
        end             
    end
end

for t=1:L
    for i=1:Ne
        if EVstatenew(t,i)>0&&Load(t,i)>0
            charging_cost=charging_cost+1;
            [Loadi,EVstatenewi]=UpdateLoad_v1(Load(:,i),EVstatenew(:,i),t);
            Load(:,i)=Loadi;
            EVstatenew(:,i)=EVstatenewi;
            %EVstatenew(t,i)=0;
            Ct(t,i)=Ct(t,i)+1;
        end
    end
end

t2=toc();

CalTime=t2-t1;

%usedwind=wind-windnew;

[ wind_use, match_degree, charging_cost] = CalZhibiao( wind(1:L,:),Ct(1:L,:) );

end
