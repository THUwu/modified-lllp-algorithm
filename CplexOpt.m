function [ charging_cost,wind_use,match_degree,Ct,CalTime ] = CplexOpt(wind, EVstate,Load )
%yalmip('clear');


[L,Ne]=size(EVstate);

Para=zeros(Ne,L,3);
NumStages=zeros(Ne,1);


x = binvar(L, Ne); %变量

C=[x<=EVstate];

for i=1:Ne
    z=0;
    ind=1;
    for j=1:L
        %C=[C,x(j,i)<=EVstate(j,i)];
        if EVstate(j,i)==1
            if z==0
                z=1;
                Para(i,ind,1)=j;
                Para(i,ind,3)=Load(j,i);
            end
        else
            if z==1
                Para(i,ind,2)=j-1;
                ind=ind+1;
                z=0;
            end
        end
    end
    NumStages(i,1)=ind;
end

w=wind;



[L,Ne]=size(EVstate);



f = sum(max(sum(x,2)-w,0)); %目标方程



for i=1:Ne
    for j=1:NumStages(i,1)
        C=[C,sum(x(Para(i,j,1):Para(i,j,2),i))==Para(i,j,3)];
    end
end

tic;

t1=toc;

optimize(C,f,[]);
%solvesdp(C,f);

t2=toc;

CalTime=t2-t1;

Ct=value(x);
charging_cost=value(f);





usedwind=zeros(L,1);
sumCt=sum(Ct,2);

for i=1:L
    if wind(i,1)>=sumCt(i,1)
        usedwind(i,1)=sumCt(i,1);
    else
        usedwind(i,1)=wind(i,1);
    end
end

wind_use=sum(usedwind)/sum(wind);

count=0;
m=0;



for i=1:L
    ma=max(wind(i,1),sumCt(i,1));
    if ma>0
        count=count+abs(wind(i,1)-sumCt(i,1))/ma;
        m=m+1;
    end
end

match_degree=1-count/m;


end

