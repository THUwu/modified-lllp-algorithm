function [ EVstate,Load,DDL,InvDDL ] = GenerateEVPath( Ne,Pro,L )



EVstate=zeros(L,Ne);
Load=zeros(L,Ne);
DDL=zeros(L,Ne);
InvDDL=zeros(L,Ne);

for i=1:Ne
    j=1;
    while j<=L-1
       r=rand(1);
       r1=ceil(rand(1)*6);
       if r>Pro(floor((j-1)/4)+1,1)
           if j+r1>=L-1
               EVstate(j:L-1,i)=1;
               r2=ceil(rand(1)*(L-1-j));
               Load(j:L-1,i)=r2;
           else
               EVstate(j:j+r1,i)=1;
               r2=ceil(rand(1)*(r1-1));
               Load(j:j+r1,i)=r2;
           end
           j=j+r1+2;         
       else           
           j=j+1;
       end
        
    end    
end

% for j=1:Ne
%     Load(L,j)=0;
%     EVstate(L,j)=0;
% end

for i=1:Ne
    count=0;
    for j=1:L
        ind=L+1-j;       
        if EVstate(ind,i)==1
            count=count+1;
            DDL(ind,i)=count;
        else
            count=0;
            DDL(ind,i)=100;
        end
  
    end
end

for i=1:Ne
    count=0;
    for j=1:L
        ind=j;       
        if EVstate(ind,i)==1
            count=count+1;
            InvDDL(ind,i)=count;
        else
            count=0;
            InvDDL(ind,i)=100;
        end
  
    end
end

end

