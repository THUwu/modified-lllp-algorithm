function [  ] = Main_stochastic( )
load('windpower.mat');
load('Pro.mat')

wind=windpower;

L=96;


NumEV=[2;3;4;5;6;7;8;9;10;20;30;40;50;60;70;80;90;100;200;300;400;500;600;700;800;900;1000];

for h=1:length(NumEV)

Ne=NumEV(h,1);

windpower=Ne*wind/10;

Ntest=100;


result_Cplex=zeros(Ntest,4);

result_nLLLP=zeros(Ntest,4);
result_nLLSP=zeros(Ntest,4);
result_nEDLL=zeros(Ntest,4);
result_nLDLL=zeros(Ntest,4);

result_non_sta=zeros(Ntest,4);

for nn=1:Ntest
    yalmip('clear');
    [ windpath,windpath_pre ] = GenerateWindPath( windpower,Ne,L );
    [ EVstate,Load,DDL,~ ] = GenerateEVPath( Ne,Pro,L );   
    
    [ result_nLLLP(nn,1),result_nLLLP(nn,2),result_nLLLP(nn,3),~,result_nLLLP(nn,4) ] = gmLLLP( windpath,windpath_pre,EVstate,Load,DDL );
    [ result_nLLSP(nn,1),result_nLLSP(nn,2),result_nLLSP(nn,3),~,result_nLLSP(nn,4) ] = gmLLSP( windpath,windpath_pre,EVstate,Load,DDL );
    [ result_nEDLL(nn,1),result_nEDLL(nn,2),result_nEDLL(nn,3),~ ,result_nEDLL(nn,4)] = gmEDLL( windpath,windpath_pre,EVstate,Load,DDL );
    [ result_nLDLL(nn,1),result_nLDLL(nn,2),result_nLDLL(nn,3),~,result_nLDLL(nn,4) ] = gmLDLL( windpath,windpath_pre,EVstate,Load,DDL );
    
     
    
    ops = sdpsettings('solver','cplex','cachesolvers',1);
    w=windpath_pre;    
    x = binvar(L, Ne); %变量
    f = sum(max(sum(x,2)-w,0)); %目标方程
    C=[x<=EVstate];

    for i=1:Ne
        [ StageMatrix,LoadVector ] = GetConstraints( EVstate(:,i), Load(:,i) );
        C=[C,StageMatrix*x(:,i)==LoadVector];
    end

    tic;
    t1=toc;
    optimize(C,f,ops);
    t2=toc;
    
    result_Cplex(nn,4)=t2-t1;
    %Ct_Cplex=value(x);
    [ result_Cplex(nn,2), result_Cplex(nn,3),result_Cplex(nn,1)] = CalZhibiao( windpath,value(x) );


    %%%%%%%%%%%%%%%%%%%%准确预测最优值
    w=windpath;    
    x = binvar(L, Ne); %变量
    f = sum(max(sum(x,2)-w,0)); %目标方程
    C=[x<=EVstate];

    for i=1:Ne
        [ StageMatrix,LoadVector ] = GetConstraints( EVstate(:,i), Load(:,i) );
        C=[C,StageMatrix*x(:,i)==LoadVector];
    end

    tic;
    t1=toc;
    optimize(C,f,ops);
    t2=toc;
    
    result_non_sta(nn,4)=t2-t1;
    %Ct_non_sta=value(x);
    [ result_non_sta(nn,2), result_non_sta(nn,3),result_non_sta(nn,1)] = CalZhibiao( windpath,value(x) );

    
end

sta=zeros(6,8);


sta(1,:)=CalSta(result_nLLLP);
sta(2,:)=CalSta(result_nLLSP);
sta(3,:)=CalSta(result_nEDLL);
sta(4,:)=CalSta(result_nLDLL);


sta(5,:)=CalSta(result_Cplex);
sta(6,:)=CalSta(result_non_sta);


filename=strcat(num2str(NumEV(h,1)),'EVs-0517.mat');
save(filename,'sta');

end

end


