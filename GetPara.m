function [ Para,NumStages ] = GetPara( EVstate, Load )
[L,Ne]=size(EVstate);

Para=zeros(Ne,L,3);
NumStages=zeros(Ne,1);

for i=1:Ne
    z=0;
    ind=1;
    for j=1:L
        if EVstate(j,i)==1
            if z==0
                z=1;
                Para(i,ind,1)=j;
                Para(i,ind,3)=Load(j,i);
            end
        else
            if z==1
                Para(i,ind,2)=j-1;
                ind=ind+1;
                z=0;
            end
        end
    end
    NumStages(i,1)=ind;
end


end

