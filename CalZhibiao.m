function [ wind_use, match_degree, charging_cost] = CalZhibiao( wind,Ct )
L=length(wind);

usedwind=zeros(L,1);
sumCt=sum(Ct,2);

charging_cost= sum(max(sumCt-wind,0));

for i=1:L
    if wind(i,1)>=sumCt(i,1)
        usedwind(i,1)=sumCt(i,1);
    else
        usedwind(i,1)=wind(i,1);
    end
end

wind_use=sum(usedwind)/sum(wind);

count=0;
m=0;



for i=1:L
    ma=max(wind(i,1),sumCt(i,1));
    if ma>0
        count=count+abs(wind(i,1)-sumCt(i,1))/ma;
        m=m+1;
    end
end

match_degree=1-count/m;


end

