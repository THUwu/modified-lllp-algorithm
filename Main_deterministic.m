function [  ] = Main_deterministic( )
load('windpower.mat');
load('Pro.mat')

wind=windpower;

L=96;

NumEV=[3];
%NumEV=[2;3;4;5;6;7;8;9;10;20;30;40;50;60;70;80;90;100;200;300;400;500;600;700;800;900;1000;2000;3000;4000;5000;6000;7000;8000;9000;10000];

for h=1:length(NumEV)

Ne=NumEV(h,1);

windpower=Ne*wind/10;

Ntest=100;


result_LLLP=zeros(Ntest,4);
result_LLSP=zeros(Ntest,4);
result_EDLL=zeros(Ntest,4);
result_LDLL=zeros(Ntest,4);
result_Cplex=zeros(Ntest,4);


for nn=1:Ntest
    yalmip('clear');
    [ windpath,~] = GenerateWindPath( windpower,Ne,L );
    [ EVstate,Load,DDL,InvDDL ] = GenerateEVPath( Ne,Pro,L );

    [ result_LLLP(nn,1),result_LLLP(nn,2),result_LLLP(nn,3),Ct_LLLP,result_LLLP(nn,4) ] = mLLLP( windpath,windpath,EVstate,Load,DDL );    
    [ result_LLSP(nn,1),result_LLSP(nn,2),result_LLSP(nn,3),Ct_LLSP,result_LLSP(nn,4) ] = mLLSP( windpath, windpath,EVstate,Load,DDL );
    [ result_EDLL(nn,1),result_EDLL(nn,2),result_EDLL(nn,3),Ct_EDLL ,result_EDLL(nn,4)] = mEDLL( windpath, windpath,EVstate,Load,DDL );
    [ result_LDLL(nn,1),result_LDLL(nn,2),result_LDLL(nn,3),Ct_LDLL,result_LDLL(nn,4) ] = mLDLL( windpath, windpath,EVstate,Load,DDL );
    
    ops = sdpsettings('solver','cplex','cachesolvers',1);
    w=windpath;


    
    x = binvar(L, Ne); %L: length of time stages Ne: number of EVs
    
    f = sum(max(sum(x,2)-w,0)); %Ŀ�귽��

    C=[x<=EVstate];

    for i=1:Ne
        [ StageMatrix,LoadVector ] = GetConstraints( EVstate(:,i), Load(:,i) );
        C=[C,StageMatrix*x(:,i)==LoadVector];
    end

    tic;
    t1=toc;
    optimize(C,f,ops);
    t2=toc;
    
    result_Cplex(nn,4)=t2-t1;
    Ct_Cplex=value(x);
    [ result_Cplex(nn,2), result_Cplex(nn,3),result_Cplex(nn,1)] = CalZhibiao( windpath,value(x) );
    
    if result_LLLP(nn,1)<result_LLSP(nn,1)
        aaa=1;
    end

end

sta=zeros(5,8);


sta(1,:)=CalSta(result_LLLP);
sta(2,:)=CalSta(result_LLSP);
sta(3,:)=CalSta(result_EDLL);
sta(4,:)=CalSta(result_LDLL);
sta(5,:)=CalSta(result_Cplex);


filename=strcat(num2str(NumEV(h,1)),'EVs-0516.mat');
save(filename,'sta');

end

end





