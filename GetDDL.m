function [ DDL ] = GetDDL( EVstate )

[L,Ne]=size(EVstate);
DDL=zeros(L,Ne);

for i=1:Ne
    count=0;
    for j=1:L
        ind=L+1-j;       
        if EVstate(ind,i)==1
            count=count+1;
            DDL(ind,i)=count;
        else
            count=0;
            DDL(ind,i)=100;
        end
  
    end
end

end

